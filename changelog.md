# Changelog

Todas as alterações do Sistema Conta serão documentadas neste arquivo.

## [5.0.000] Lançamento Oficial - Previsão: 01/10/2019

### Implementação
- CRUD para alteração do Instrumento Inicial para correção de erros digitação;
- CRUD para cadastramento de Prepostos do Contrato;
- CRUD para cadastramento dos Itens do Contrato;
- Data Início e Data Fim para Responsáveis de Contratos;
- Cronograma de Desembolso Financeiro do Contrato;
- Todas as funcionalidades do Sistema Conta Versão 4.1.11 (Framework Scriptcase);

### Descontinuada
- Módulo TED

### Bug
- Correção do cálculo do Valor Acumulado atualizado por meio do Cronograma (bug da versão 4.1.11).
- Alteração do campo receita_despesa da tabela contratohistorico para CHAR(1).

### Removida
- Nenhuma

### Segurança
- Alteração tipo de criptocrafia da senha do usuário
- Novo Framework - Laravel 5.7


